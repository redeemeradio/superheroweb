<!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container" style='padding-right: 0px;'>
            <div class="navbar-header">

                <button style='padding-top: 7px;' type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="glyphicon glyphicon-align-justify" style='color: #fff;'></i>
                </button>

                <!--<label id='ip-surf' style='float: right; margin: 15px 0px 0px 10px; font-size: .9em; font-weight:normal;'>
                    <input id='ip-surf-checked' type='checkbox' />IP<span id='ip-surf-mirror'>-mirror</span>
                </label>-->

                <select id='select-mirror' onchange='SuperHeroFM.mirror();' style='float: right; margin: 16px 0px 0px 0px; max-width: 64px; color: #000; font-size: .85em;'>
                    <option>mirror</option>
                    <option data-ip="104.194.232.247" data-hostname="www.superherofm.com">www.superherofm.com (global) SSL: on</option>
                    <option data-ip="104.194.232.247" data-hostname="www.anonyproxi.es">www.anonyproxi.es (global) SSL: on</option>
                    <option data-ip="104.194.232.247" data-hostname="www.superherofm.com">www.superherofm.com (global) SSL: on</option>
                    <option data-ip="104.194.232.247" data-hostname="www.moshiachradio.com">www.moshiachradio.com (global) SSL: on</option>
                    <option data-ip="104.194.232.247" data-hostname="www.redeemeradio.com">www.redeemeradio.com (global) SSL: on</option>
                    <option data-ip="github.com" data-hostname="www.github.com/superherofm/superheroweb">+ [Add a mirror]</option>
                    <!-- 104.219.54.54 -->

                </select>

                <a class="navbar-brand page-scroll" href="#page-top" style=''>
                    <i style='font-size:.9em;top:0px;left:2px;position:relative;' class="glyphicon glyphicon-sound">&#x1F50A;</i>  <span style='color:gold;'>Super</span><span style='color:;'>Hero</span><span style='color:gold;'>FM</span>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>

                    <li>
                        <a class="page-scroll" id='nav_login_link' onclick='SuperHeroFM.modal_login(); return false;' href="#">Login</a>
                    </li>

                    <li>
                        <a class="page-scroll" id='nav_about_link' href="#about">About</a>
                    </li>

                    <li>
                        <a class="page-scroll" href="#anonymailer">Email</a>
                    </li>

                    <li>
                        <a class="page-scroll" id='nav_contact_link' href="#contact">Contact</a>
                    </li>
                    
                    <li>
                        <a class="page-scroll" id='nav_terms_link' href="/team.html">Team</a>
                    </li>
                    
                    <li>
                        <a class="page-scroll" id='nav_terms_link' href="#terms">Terms</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>