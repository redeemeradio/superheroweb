    var SuperHeroFM = this.SuperHeroFM = {

        //modal init
        mirror: function () {
            var selection = $('#select-mirror');
            var option_select = selection.find('option:selected');
            data_hostname = option_select.attr('data-hostname');
            data_ip = option_select.attr('data-ip');
            var timer_loading = setTimeout(function () {
                $('<option>loading...</option>').appendTo(selection);
                selection.val('loading...');
            }, 1000);
            var timer_redirect = setTimeout(function () {
                if ($('#ip-surf-checked').is(':checked'))
                    window.location = 'http://' + data_ip;
                else
                    window.location = 'http://' + data_hostname;
            }, 2000);

        },

        modal_login: function () {

            if (window.localStorage["email"]) {
                $('#superherofm_user_form_username').val(window.localStorage["email"]);
            }
            if (window.localStorage["password"]) {
                $('#superherofm_user_form_password').val(window.localStorage["password"]);
            }
            $('#modal-login').bPopup();

        },

        modal_stripe: function (id, pricing, label, plan) {

            $(id).bPopup({
                follow: false
            });
            $(id).find('.modal-pricing').html(pricing);
            $(id).find('.modal-label').html(label);
            $(id).find('#plan').val(plan);

        },

        //billing

        stripe_init_form: function () {

            $('input[data-stripe="number"]').payment('formatCardNumber');
            $('input[data-stripe="cvc"]').payment('formatCardCVC');

            $('input[data-stripe="number"]').blur(function () {
                var card_type = $.payment.cardType($('input[data-stripe="number"]').val());
                if (card_type)
                    $('.input-group-cc').html(card_type);
            });

            if (window.localStorage["1weeklyvpn-token"]) {
                $('#btn-1weeklyvpn-access').show();
                $('#btn-1weeklyvpn-subscribe').hide();
            }

            if (window.localStorage["1weeklyproxy-token"]) {
                $('#btn-1weeklyproxy-access').show();
                $('#btn-1weeklyproxy-subscribe').hide();
            }

            if (window.localStorage["1weeklypptp-token"]) {
                $('#pptp-username').attr('type', 'text');
                $('#pptp-password').attr('type', 'text');
                $('#btn-1weeklypptp-subscribe').hide();
            }

            $('button#cc_submit').on('touchend', function (e) {
                $('#superherofm_stripe').submit();
            });


            $('#superherofm_stripe').submit(function (e) {

                // show card type: 
                var card_type = $.payment.cardType($('input[data-stripe="number"]').val());
                $('.input-group-cc').html(card_type);

                // run the post
                SuperHeroFM.stripe(this, '#superherofm_stripe input[data-stripe="email"]', $('#plan').val());

                return false;

            });

        },

        stripe: function (form_selector, email_field, plan) {

            var $form = $(form_selector);
            var number = $form.find('input[data-stripe="number"]').val();
            var email = $(email_field).val();

            $form.find('button').prop('disabled', true);

            // post to stripe get a card token back
            Stripe.card.createToken($form, function (status, response) {
                if (response.error) {
                    // Show the errors on the form
                    alert(response.error.message);
                    $form.find('button').prop('disabled', false);
                } else {
                    // response contains id and card, which contains additional card details
                    var token = response.id;
                    var last_four = number.slice(-4);

                    //generate a password
                    password = Math.floor(Math.random() * 9000) + 1000;

                    // Insert the token into the form so it gets submitted to the server
                    $form.append($('<input type="hidden" name="stripeToken" />').val(token));

                    $.post("/api/users/create_user_token/", {
                            "username": email,
                            "email": email,
                            "password": password,
                            "token": token,
                            "plan": plan,
                            "last_four": last_four,
                        },
                        function (data) {
                            if (data.success) {
                                alert("Your username and password are now created!  Write down your password! \n\nUsername: " + email + "\nPassword: " + password);

                                // store the token email and pass
                                window.localStorage["token"] = token;
                                window.localStorage["email"] = email;
                                window.localStorage["password"] = data.password;
                                window.localStorage["last_four"] = last_four;
                                window.localStorage["user_stripe_customer_id"] = data.user_stripe_customer_id;
                                window.localStorage["user_id"] = data.user_id;

                                // store the token and password for the plan
                                window.localStorage[plan + "-token"] = token;
                                window.localStorage[plan + "-email"] = email;
                                window.localStorage[plan + "-password"] = data.password;
                                window.localStorage[plan + "-last_four"] = last_four;
                                window.localStorage[plan + "-user_stripe_customer_id"] = data.user_stripe_customer_id;
                                window.localStorage[plan + "-user_id"] = data.user_id;

                                // close the modal
                                $('.b-close').click();

                                // launch the vpn details modal
                                $('#modal-' + plan + '-access').bPopup({
                                    follow: false
                                });

                                // show the button
                                $('#btn-' + plan + '-subscribe').hide();
                                $('#btn-' + plan + '-access').show();
                            } else {
                                alert(data.error_msg);
                            }



                        }
                    );




                    // and submit
                    // $form.get(0).submit();
                }

                //reset the card type:
                $('.input-group-cc').html("<i class='glyphicon glyphicon-ok'></i>");

            });
            return false;
        },


        init_dropdown: function () {
            $(".dropdown-menu li a").click(function () {
                var selText = $(this).text();
                $('.dropdown-toggle').html(selText + ' <span class="caret"></span>');

                // global url
                var global_url = $('#form-global-url').val();

                // conditionals
                if (selText == "CGIProxy") {

                }

            });


        },

        init_onsubmit: function () {
            var global_url = $('#form-global-url').val();
            var service_text = $('.dropdown-toggle').text().trim();
            var is_search = $('#form-global-search').is(':checked');

            var global_search_url = 'http://m.search.aol.com/search?s_it=topsearchbox.nrf&v_t=na&q=';

            if (service_text == 'CGIProxy') {
                //set CGIProxy form to the URL value and submit it!
                if (is_search) {
                    var search_string = encodeURI(global_search_url + global_url);
                    $('form#cgiproxy').find('input[name="URL"]').val(search_string);
                } else {
                    $('form#cgiproxy').find('input[name="URL"]').val(global_url);
                }
                $('form#cgiproxy').submit();

            }
            if (service_text == 'A2') {
                //set A2 form to the URL value and submit it!
                if (is_search) {
                    var search_string = encodeURIComponent('https://duckduckgo.com/html/?q=' + global_url);
                    window.location = '/a2/index.php?q=' + search_string + '&hl=5c5';
                } else {
                    window.location = '/a2/index.php?q=' + encodeURIComponent(global_url) + '&hl=5c5';
                }

            }
            if (service_text == 'Glype') {
                //set Glype form to the URL value and submit it!
                if (is_search) {
                    var search_string = encodeURI(global_search_url + global_url);
                    $('form#glype').find('input[name="u"]').val(search_string);
                } else {
                    $('form#glype').find('input[name="u"]').val(global_url);
                }
                $('form#glype').submit();
            }
            if (service_text == 'PHProxy') {
                //set Glype form to the URL value and submit it!
                if (is_search) {
                    var search_string = encodeURI(global_search_url + global_url);
                    $('form#phproxy').find('input[id="address_box"]').val(search_string);
                } else {
                    $('form#phproxy').find('input[id="address_box"]').val(global_url);
                }
                $('form#phproxy').submit();
            }
            if (service_text == 'PHProxy++') {
                //set Glype form to the URL value and submit it!
                if (is_search) {
                    var search_string = encodeURI(global_search_url + global_url);
                    $('form#phproxy_plus_plus').find('input[name="u"]').val(search_string);
                } else {
                    $('form#phproxy_plus_plus').find('input[name="u"]').val(global_url);
                }
                $('form#phproxy_plus_plus').submit();
            }
            if (service_text == 'v3.2b2') {
                //set Glype form to the URL value and submit it!
                if (is_search) {
                    var search_string = encodeURI(global_search_url + global_url);
                    $('form#v32b2').find('input[name="url"]').val(search_string);
                } else {
                    $('form#v32b2').find('input[name="url"]').val(global_url);
                }
                $('form#v32b2').submit();
            }

            return false;
        },

        init_enter: function () {
            $('input#form-global-url').keypress(function (e) {
                if (e.which == 13) {
                    $('form#global_form').submit();
                    return false; //<---- Add this line
                }
            });

        },

        init_search: function () {

            $('input[type="checkbox"]#form-global-search').on('click', function (e) {
                if ($(this).is(":checked")) {
                    $("#form-global-url").val("").focus();
                } else {
                    $("#form-global-url").val("http://").focus();
                }

            });

            $('span#form-global-search-span').on('click touchstart', function (e) {
                if ($(this).find('input[type="checkbox"]').is(':checked') && $('input#form-global-url').val() != '') {
                    if (!$(this).is(':checked'))
                        SuperHeroFM.init_onsubmit();
                }
            });


        },

        init: function () {
            SuperHeroFM.init_dropdown();
        }


    };

    $(document).ready(function () {
        SuperHeroFM.init();
        SuperHeroFM.init_enter();
        SuperHeroFM.init_search();

        SuperHeroFM.stripe_init_form();
        var timeoutId;
        $('.btn-circle').mousedown(function () {
            timeoutId = setTimeout(function () {
                window.localStorage.clear();
                alert('localstore cleared!');
                window.location = 'index.html';
            }, 5000);
        }).bind('mouseup mouseleave', function () {
            clearTimeout(timeoutId);
        });

        var iOS = (navigator.userAgent.match(/iPad|iPhone|iPod/g) ? true : false);

        if (iOS) {
            $('.ios-hide').hide();
        }

    });

    function addUrl(url) {
        document.getElementById('address_box').value = url;
    }

    function showOptions() {
        document.getElementById('options').style.display = 'show';
    }

    function form_submit() {
        init_form_submit();
    }
